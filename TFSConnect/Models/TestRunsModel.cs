﻿using Microsoft.TeamFoundation.TestManagement.WebApi;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TFSConnect.Models
{
    [DataContract]
    public class TestRunsModel
    {
        public List<TestRun> value { get; set; }
        public int count { get; set; }
    }
}
