﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TFSConnect.Models
{
    [DataContract]
     public class PostTestRunModel
    {
        [DataMember]
        public string name { get; set; }

        [DataMember]
        public Plan plan { get; set; }

        [DataMember]
        public bool isAutomated { get; set; }
        [DataMember]
        public List<int> pointIds { get; set; }
    }

    [DataContract]
    public class Plan
    {
        [DataMember]
        public string id { get; set; }
    }

}
