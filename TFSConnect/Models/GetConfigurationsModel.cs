﻿using Microsoft.TeamFoundation.TestManagement.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TFSConnect.Models
{
    [DataContract]
    public class ConfigurationsModel
    {
        /// <summary>
        /// List of Test Configurations.
        /// Note: couldn't get datamember attribute Name="ConfigurationList" to work so have to go with "value"
        /// </summary>
        public List<TestConfiguration> value { get; set; }
        public int count { get; set; }
    }
        
}
