﻿using Microsoft.TeamFoundation.TestManagement.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TFSConnect.Models
{
    [DataContract]
    public class PostResultAsListModel
    {
        [DataMember]
        public List<TestResultCreateModel> testResults { get; set; }
    }
}
