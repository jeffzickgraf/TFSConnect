﻿using Microsoft.TeamFoundation.TestManagement.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TFSConnect.Models
{
    [DataContract]
    public class TestVariablesModel
    {
        public List<TestVariable> value { get; set; }
        public int count { get; set; }
    }

      
}
