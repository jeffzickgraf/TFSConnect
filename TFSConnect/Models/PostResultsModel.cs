﻿using Microsoft.TeamFoundation.TestManagement.WebApi;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TFSConnect.Models
{
    [DataContract]
    public class PostResultsModel
    {
        [DataMember]
        public List<PostResult> testResults { get; set; }
    }

    [DataContract]
    public class PostResult
    {
        [DataMember]
        public string testCaseTitle { get; set; }
        [DataMember]
        public string automatedTestName { get; set; }
        [DataMember]
        public int priority { get; set; }
        [DataMember]
        public string outcome { get; set; }
        //[DataMember]
        //public List<AssociatedBug> associatedBugs { get; set; }
    }

    [DataContract]
    public class AssociatedBug
    {
        [DataMember]
        public int id { get; set; }
    }
}
