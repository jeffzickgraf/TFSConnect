﻿using Microsoft.TeamFoundation.TestManagement.WebApi;
using Microsoft.VisualStudio.Services.Client;
using Microsoft.VisualStudio.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using TFSConnect.Models;
using TFSConnect.TFSApi;

namespace TFSConnect
{
    class Program
    {
        static void Main(string[] args)
        {
            string userDomain =  Environment.UserDomainName;
            string user = Environment.UserName;
            BasicAuthRestSample();
        }

        public static void BasicAuthRestSample()
        {
            try
            {
                //"http://vsalm:8080/tfs/FabrikamFiberCollection/TFSStarter"
                Uri collectionUri = new Uri("http://vsalm:8080/tfs/FabrikamFiberCollection");
                // Create instance of VssConnection using basic auth credentials. 
                // For security, ensure you are connecting to an https server, since credentials get sent in plain text.
                VssConnection connection = new VssConnection(collectionUri, new VssCredentials(new WindowsCredential(new NetworkCredential("Julia", "P2ssw0rd"))));
                string baseProjectUri = "http://vsalm:8080/tfs/FabrikamFiberCollection/TFSStarter";
                TestPlans plans = new TestPlans(connection, baseProjectUri);
                var testPlans = plans.GetTestPlans();

                TestSuites suites = new TestSuites(connection, baseProjectUri);
                var testSuites = suites.GetTestSuites(testPlans.First().Id);

                string projectId = testPlans.First().Project.Id;

                TestPoints points = new TestPoints(connection, baseProjectUri);
                var testPoints = points.GetTestPoints(testPlans.First().Id, testSuites.First().Id);

                TestRuns runs = new TestRuns(connection, baseProjectUri);
                var testRuns = runs.GetTestRuns();
                
                //Run automation
                //Create a new TestRun
                //with TestResult
                //For TestPoints --TestRun object doesn't have point id's so not sure if needed

                RunCreateModel myRun = new RunCreateModel
                (
                    name: "Perfecto test run",
                    isAutomated: true,
                    buildId: 100012,
                    buildPlatform: "Debug",
                    buildFlavor: "Any CPU",
                    startedDate: DateTime.UtcNow.ToString()
                );
               
                //PostTestRunModel myRun = new PostTestRunModel();
                //Models.Plan myPlan = new Plan();
                //myPlan.id = testPlans.First().Id.ToString();
                //myRun.plan = myPlan;
                //myRun.name = "Perfecto Test Run";
                //myRun.isAutomated = true;
                //myRun.pointIds = testPoints.Select(s => s.Id).ToList();
               
                connection = new VssConnection(collectionUri, new VssCredentials(new WindowsCredential(new NetworkCredential("Julia", "P2ssw0rd"))));
                TestRuns postToRuns = new TestRuns(connection, baseProjectUri);
                TestRun postedRun = postToRuns.PostTestRun(myRun).Result;
                
                int runId = postedRun.Id;
                TestResultCreateModel[] caseResults = new TestResultCreateModel[testPoints.Count];
                int resultCounter = 0;
                foreach (var point in testPoints)
                {
                    var result = new TestResultCreateModel();

                    result.StartedDate = DateTime.UtcNow.ToString();
                    result.CompletedDate = DateTime.UtcNow.AddSeconds(1).ToString();
                    result.TestPoint = new ShallowReference(point.Id.ToString());
                    result.Configuration = new ShallowReference(point.Configuration.Id, point.Configuration.Name);
                    result.Outcome = "Failed";
                    result.ErrorMessage = "Put_an_error_message_here";
                    result.State = "5";
                    result.TestCasePriority = "1";
                    if (point.TestCase.Id == "252")
                    {
                        result.TestCaseTitle = "Native010_SignIn";
                        result.AutomatedTestName = "VSTSDigitalDemoTests.TestCases.AppiumStarbucksTests.AppiumStarbucksTests.Native010_SignIn";
                    }
                    else
                    {
                        result.TestCaseTitle = "Native20_Logout";
                        result.AutomatedTestName = "VSTSDigitalDemoTests.TestCases.AppiumStarbucksTests.AppiumStarbucksTests.Native20_Logout";
                    }
                    result.TestCase = new ShallowReference(point.TestCase.Id.ToString());
                    caseResults[resultCounter] = result;
                    resultCounter++; 
                }

                connection = new VssConnection(collectionUri, new VssCredentials(new WindowsCredential(new NetworkCredential("Julia", "P2ssw0rd"))));
                TestResults testResults = new TestResults(connection, baseProjectUri);
                List<TestCaseResult> testCaseResults = testResults.PostTestResult(runId, projectId, caseResults).Result;

                RunUpdateModel runUpdateModel = new RunUpdateModel(
                  state: TestRunState.Completed.ToString(),
                  completedDate: DateTime.UtcNow.ToString());

                TestRun updatedRun = postToRuns.PostUpdateRun(runUpdateModel, runId, projectId).Result;
            }
            catch (Exception ex)
            {
                throw;
            }
            
           }
    }
}
