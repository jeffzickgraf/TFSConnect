﻿using Microsoft.TeamFoundation.Test.WebApi;
using Microsoft.TeamFoundation.TestManagement.WebApi;
using Microsoft.VisualStudio.Services.Client;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using TFSConnect.Models;


namespace TFSConnect.TFSApi
{
    public class TestSuites : TFSApiBase
    {
        public TestSuites(VssConnection connection, string projectUri)
        {
            _connection = connection;
            _projectUri = projectUri;
        }
        public List<TestSuite> GetTestSuites(int planId)
        {
            TestHttpClient testClient = _connection.GetClient<TestHttpClient>();
            Task<HttpResponseMessage> responseTask 
                = testClient.HttpClient.GetAsync(string.Format("{0}/_apis/test/plans/{1}/suites?api-version=3.0-preview",
                    _projectUri, planId));
            HttpResponseMessage response = responseTask.Result;
            var executionJson = response.Content.ReadAsStringAsync().Result;
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

            return jsonSerializer.Deserialize<TestSuitesModel>(executionJson).value;
        }
    }
}
