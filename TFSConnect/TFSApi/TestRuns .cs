﻿using Microsoft.TeamFoundation.Test.WebApi;
using Microsoft.TeamFoundation.TestManagement.WebApi;
using Microsoft.VisualStudio.Services.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using TFSConnect.Models;


namespace TFSConnect.TFSApi
{
    public class TestRuns : TFSApiBase
    {
        public TestRuns(VssConnection connection, string projectUri)
        {
            _connection = connection;
            _projectUri = projectUri;
        }
        public List<TestRun> GetTestRuns()
        {
            using (TestHttpClient testClient = _connection.GetClient<TestHttpClient>())
            {
                Task<HttpResponseMessage> responseTask
                    = testClient.HttpClient.GetAsync(string.Format("{0}/_apis/test/runs?api-version=3.0-preview",
                        _projectUri));
                HttpResponseMessage response = responseTask.Result;
                var executionJson = response.Content.ReadAsStringAsync().Result;
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

                return jsonSerializer.Deserialize<TestRunsModel>(executionJson).value;
            }
        }

        public async Task<TestRun> PostTestRun(RunCreateModel testRun)
        {
            using (TestHttpClient testClient = _connection.GetClient<TestHttpClient>())
            {
                var stringPayload = Task.Run(() => JsonConvert.SerializeObject(testRun)).Result;
                HttpContent content = new StringContent(stringPayload, Encoding.UTF8, "application/json");

                using (HttpResponseMessage response
                    = testClient.HttpClient.PostAsync(string.Format("{0}/_apis/test/runs?api-version=3.0-preview",
                        _projectUri), content).Result)
                {
                    //HttpResponseMessage response = responseTask.Result;
                    var executionJson = await response.Content.ReadAsStringAsync();
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

                    return jsonSerializer.Deserialize<TestRun>(executionJson);
                }
            }
        }

        public async Task<TestRun> PostUpdateRun(RunUpdateModel testRunToUpdate, int runId, string projectId)
        {
            using (TestManagementHttpClient testmanagementClient = _connection.GetClient<TestManagementHttpClient>())
            {
                var run = testmanagementClient.UpdateTestRunAsync(projectId, runId, testRunToUpdate);
                return await run;
            }
        }
    }
}
