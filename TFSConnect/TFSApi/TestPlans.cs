﻿using Microsoft.TeamFoundation.Test.WebApi;
using Microsoft.TeamFoundation.TestManagement.WebApi;
using Microsoft.VisualStudio.Services.Client;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using TFSConnect.Models;

namespace TFSConnect.TFSApi
{
    public class TestPlans : TFSApiBase
    {
        public TestPlans(VssConnection connection, string projectUri)
        {
            _connection = connection;
            _projectUri = projectUri;
        }
        public List<TestPlan> GetTestPlans()
        {
            TestHttpClient testClient = _connection.GetClient<TestHttpClient>();
            Task<HttpResponseMessage> responseTask = testClient.HttpClient.GetAsync(string.Format("{0}/_apis/test/plans?api-version=3.0-preview", _projectUri));
            HttpResponseMessage response = responseTask.Result;
            var executionJson = response.Content.ReadAsStringAsync().Result;
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            
            return jsonSerializer.Deserialize<TestPlansModel>(executionJson).value;
        }
    }
}
