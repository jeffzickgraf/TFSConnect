﻿using Microsoft.TeamFoundation.Test.WebApi;
using Microsoft.VisualStudio.Services.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using TFSConnect.Models;

namespace TFSConnect.TFSApi
{
    public class ConfigurationVariables : TFSApiBase
    {
        public ConfigurationVariables(VssConnection connection, string projectUri)
        {
            _connection = connection;
            _projectUri = projectUri;
        }        

        public TestVariablesModel GetConfigurationVariables()
        {
            TestHttpClient testClient = _connection.GetClient<TestHttpClient>();
            Task<HttpResponseMessage> responseTask = testClient.HttpClient.GetAsync(string.Format("{0}/_apis/test/variables?api-version=3.0-preview", _projectUri));
            HttpResponseMessage response = responseTask.Result;
            var executionJson = response.Content.ReadAsStringAsync().Result;
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

            return jsonSerializer.Deserialize<TestVariablesModel>(executionJson);
        }
    }
}
