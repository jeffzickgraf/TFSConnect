﻿using Microsoft.TeamFoundation.Test.WebApi;
using Microsoft.TeamFoundation.TestManagement.WebApi;
using Microsoft.VisualStudio.Services.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using TFSConnect.Models;


namespace TFSConnect.TFSApi
{
    public class TestResults : TFSApiBase
    {
        public TestResults(VssConnection connection, string projectUri)
        {
            _connection = connection;
            _projectUri = projectUri;
        }
       
        public async Task<List<TestCaseResult>> PostTestResult(int runId, string projectId, TestResultCreateModel[] createModel)
        {
            using (TestManagementHttpClient testClient = _connection.GetClient<TestManagementHttpClient>())
            {
                var results = testClient.AddTestResultsToTestRunAsync(createModel, projectId, runId);
                return await results;
            }
        }
    }
}
