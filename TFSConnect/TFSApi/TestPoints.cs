﻿using Microsoft.TeamFoundation.Test.WebApi;
using Microsoft.TeamFoundation.TestManagement.WebApi;
using Microsoft.VisualStudio.Services.Client;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using TFSConnect.Models;


namespace TFSConnect.TFSApi
{
    public class TestPoints : TFSApiBase
    {
        public TestPoints(VssConnection connection, string projectUri)
        {
            _connection = connection;
            _projectUri = projectUri;
        }
        public List<TestPoint> GetTestPoints(int planId, int suiteId)
        {
            TestHttpClient testClient = _connection.GetClient<TestHttpClient>();
            Task<HttpResponseMessage> responseTask 
                = testClient.HttpClient.GetAsync(string.Format("{0}/_apis/test/plans/{1}/suites/{2}/points?api-version=3.0-preview&includePointDetails=true", 
                _projectUri, planId, suiteId));
            HttpResponseMessage response = responseTask.Result;
            var executionJson = response.Content.ReadAsStringAsync().Result;
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

            return jsonSerializer.Deserialize<TestPointsModel>(executionJson).value;
        }
    }
}
